class Board < ActiveRecord::Base
  after_initialize :init

  def init
    self.columns ||= 7 if self.has_attribute? :columns
    self.rows ||= 6 if self.has_attribute? :rows
  end
end
