class GameEndsHasWinner
class GameEndsInDraw

token_colors = [ "red", "yellow" ]
current_color_idx = 1 if not current_color_idx?
turn_number = 0 if not turn_number?
total_rows = 0

switch_players = ->
    current_color_idx = if current_color_idx == 1 then 0 else 1
    $("#message").text "#{ current_token_color().toUpperCase() }'s turn to move."
    swap_token_space_color(current_token_color())

current_token_color = -> token_colors[current_color_idx]

swap_token_space_color = (token_color) ->
    swap_token_color(token, token_color) for token in $("#token-space .board-column .board-row img") when $(token).attr("data-eg-full") == "false"

swap_token_color = (target_token, token_color) ->
    target_token.src = image_path "token-#{ token_color }.svg"

next_available_slot = (column_idx) ->
    slots = $($("#board-container .board-column")[column_idx]).find(".board-row img[data-eg-owner='none']")
    slots[slots.length - 1]

place_token = (column_idx, token_color) ->
    target_slot = next_available_slot column_idx
    if target_slot?
        swap_token_color(target_slot, token_color)
        $(target_slot).attr "data-eg-owner", current_token_color()

is_winning_condition = (slots) ->
    slot_set = if slots instanceof JS.SortedSet then slots else new JS.SortedSet slots
    if slot_set.count() >= 4
        slot_set.xor([slot_set.min()..slot_set.max()]).length == 0
    else
        false

check_for_winner = ->
    player_slots = $("#board-container img[data-eg-owner='#{ current_token_color() }']")

    # Construct maps
    [map_row, map_col]  = [[], []]

    for slot in player_slots
        col_idx = $(slot).parent().parent().index()
        row_idx = $(slot).parent().index()

        if map_row[row_idx]?
            map_row[row_idx].add col_idx
        else
            map_row[row_idx] = new JS.SortedSet [col_idx]

        if map_col[col_idx]?
            map_col[col_idx].add row_idx
        else
            map_col[col_idx] = new JS.SortedSet [row_idx]

    # Check by rows & cols
    for map in [map_row, map_col]
        for idx, slots of map
            slots.forEachCons 4, (cons) ->
                throw new GameEndsHasWinner if is_winning_condition cons

    # Check diagonally
    idx_set = new JS.SortedSet (Math.round row_idx for row_idx of map_row)
    idx_set.forEachCons 4, (cons) ->
        if is_winning_condition cons
            points = new JS.Set
            for idx in [cons[0]..cons[cons.length - 1]]
                map_row[idx].forEach (row) ->
                    points.add(new Point(row, idx))
            neighbors = new JS.Set
            points.forEach (o_point) ->
                points.forEach (i_point) ->
                    return if i_point == o_point
                    if Math.round(Math.pow(o_point.distanceTo(i_point), 2)) == 2
                        neighbors.add(i_point)
                        neighbors.add(o_point)

            ## y - y1 = m(x - x1)  ==  y = mx + b
            # m == -1 always for valid input in L->R config
            neighbors_lr = neighbors.classify (point) -> point.y - (point.x * -1)
            # m == 1 always for valid input in R->L config
            neighbors_rl = neighbors.classify (point) -> point.y - (point.x * 1)

            for neighbor_set in [neighbors_lr, neighbors_rl]
                neighbor_set.forEachValue (points) ->
                    if points.length >= 4
                        console.log "#{points}"
                        throw new GameEndsHasWinner
                    
    false

disable_board = ->
    $(column).removeClass "hover" for column in $("#token-space .board-column")
    $(".board-column").off "click"
    $(".page").off "mouseover"
    $(".page").off "mouseout"

$ ->
    total_rows = $("#board-container .board-row").length

    for key, value of window.image_paths
        dummy = new Image();
        dummy.src = value

    switch_players()

    $(".page").on "mouseover", ".board-column", (event) ->
        if not next_available_slot($(this).index())?
            $(this).find("img").attr "data-eg-full", true
            swap_token_color $("#token-space .board-column img")[$(this).index()], "full"
        $($("#token-space .board-column")[$(this).index()]).addClass "hover"

    $(".page").on "mouseout", ".board-column", (event) ->
        $($("#token-space .board-column")[$(this).index()]).removeClass "hover"

    $(".board-column").on "click", ".board-row", (event) ->
        if $(this).find("img").attr("data-eg-full") == "true"
            return

        turn_number++
        place_token($($(this).parent()).index(), current_token_color())

        try
            throw GameEndsInDraw if turn_number >= total_rows
            check_for_winner() if turn_number >= 7

            switch_players()
            if not next_available_slot($(this).parent().index())?
                $(this).find("img").attr "data-eg-full", true
                swap_token_color $("#token-space .board-column img")[$(this).parent().index()], "full"
        catch err
            if err instanceof GameEndsHasWinner
                $("#message").text "#{ current_token_color().toUpperCase() } wins after #{ turn_number } turns."
            else if err instanceof GameEndsInDraw
                $("#message").text "Game ends in a draw. Reload for a new round."
            else
                throw err
            disable_board()
