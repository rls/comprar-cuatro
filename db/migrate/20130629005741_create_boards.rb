class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.integer :columns
      t.integer :rows

      t.timestamps
    end
  end
end
